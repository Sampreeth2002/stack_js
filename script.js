var inputData = document.querySelector(".numinput");
var push = document.querySelector(".push");
var pop = document.querySelector(".pop");
var clearStack = document.querySelector(".clear");
var elements = document.querySelector(".elements");
var showStack = document.querySelector(".showStack");
var previousElement = document.querySelector(".previousElement");
var page1 = document.querySelector(".page1");
var page2 = document.querySelector(".page2");
var int = document.querySelector(".int");
var char = document.querySelector(".char");

class Stack {
  constructor() {
    this.items = [];
    this.count = 0;
  }

  push(element) {
    this.items[this.count] = element;
    this.count += 1;
  }

  pop() {
    if (this.count == 0) return "Stack Is Empty";
    let deleteItem = this.items[this.count - 1];
    this.count -= 1;
    return deleteItem;
  }

  print() {
    let str = "";
    if (this.count == 0) {
      str = `<div class="col">` + "Stack is Empty" + "</div>";
    }

    for (let i = this.count - 1; i > -1; i--) {
      str += `<div class="circle">` + this.items[i] + "</div>";
    }
    return str;
  }

  clear() {
    this.items = [];
    this.count = 0;
  }
}

var popElement;
var typeSelected;

const stack = new Stack();
page2.classList.add("display");

int.addEventListener("click", () => {
  page1.classList.add("display");
  page2.classList.remove("display");
  typeSelected = "number";
  inputData.type = "number";
});

char.addEventListener("click", () => {
  page1.classList.add("display");
  page2.classList.remove("display");
  inputData.type = "text";
  inputData.pattern = "[a-zA-Z]+";
});

elements.classList.add("display");
previousElement.classList.add("display");

const stackElements = () => {
  elements.innerHTML = "";
  elements.innerHTML = "<div class='stacked'>STACK ELEMENTS</div>";
};

const popedElements = () => {
  previousElement.innerHTML =
    "<div class='history poped '>POPED ELEMENTS</div>";
};

stackElements();
popedElements();

push.addEventListener("click", () => {
  var ans = inputData.value;
  stack.push(ans);
  stackElements();
  elements.innerHTML += stack.print();
});

pop.addEventListener("click", () => {
  popElement = stack.pop();
  stackElements();
  if (popElement != "Stack Is Empty") {
    elements.innerHTML += stack.print();
    previousElement.innerHTML +=
      "<div class='history circle2'>" + popElement + "</div>";
  }
});

clearStack.addEventListener("click", () => {
  stack.clear();
  elements.innerHTML = stack.print();
  stackElements();
  popedElements();
});

showStack.addEventListener("click", () => {
  elements.classList.toggle("display");
  previousElement.classList.toggle("display");
  if (showStack.textContent == "Show Stack") {
    showStack.textContent = "Hide Stack";
  } else {
    showStack.textContent = "Show Stack";
  }
});
